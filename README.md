# Mercedes Benz EV showcase
Choose a self-explaining name for your project.

## Description
Interaction module of the interactive experience for Mercedes Benz EV, Helsinki.
This module makes uses a fork of [Antoine Lamé's Gaze Tracking](https://github.com/antoinelame/GazeTracking) and the [fitbit Web API](https://dev.fitbit.com/build/reference/web-api/)


## Installation
* This project uses the [Anaconda pacakge manager](https://www.anaconda.com/). Follow [Anaconda's installation instructions](https://www.anaconda.com/products/distribution) to install it on your system.
* Clone this repository and its submodule:
```
git clone --recurse-submodules -j8 https://gitlab.com/krrnk/mercedes_benz.git
```
* Change directory to this project:
```
cd mercedes_benz

```
* Install the conda environment:
```
conda env create -f mercedesenv.yml
```
* Activate the environment:
```
conda activate mercedesenv
```
* Test if python-osc is installed:
```
python
>> import pythonosc
```
If you encounter this error `ModuleNotFoundError: No module named 'pythonosc'`, you can solve it by running `pip install python-osc` while the mercedesenv is active.

## Usage
With the mercedesenv activated run:
```
python main.py
```
* By default the default computer webcam is in use. This can be changed by editing line 30 of `main.py`:
```
webcam = cv2.VideoCapture(0) # change from 0 to 1 to use a secondary webcam
```

* OSC messages are sent to the localhost on the port 5005 with the addr '/gaze_vel'. This can be changed by editing line 42 of `main.py`:
```
send_osc('127.0.0.1', 5005, '/gaze_vel', user_activity)
```

## Support
Feel free to contribute or raise an issue.

## Authors and acknowledgment
This project uses a fork of [Antoine Lamé's Gaze Tracking](https://github.com/antoinelame/GazeTracking).

## License
This project is released by krrnk under the terms of the MIT Open Source License. View LICENSE for more information.

## Project status
Under development.
