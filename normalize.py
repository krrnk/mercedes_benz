def clip(in_val, min_val, max_val):
    return sorted([min_val, in_val, max_val])[1]

def normalize(in_value, min_val, max_val):
    return (in_value - min_val) / (max_val - min_val)

if __name__ == '__main__':
    in_value = int(input('what is the current value'))
    result = clip(0, normalize(in_value, 60, 120), 1)
    print(f'result is: {result}')
