import requests
import json
import keyboard
import time

def get_request(url, token):
    res = requests.get(url, headers={
        'Authorization': 'Bearer ' + token
    })
    return res.json()

def current_heart_rate(response):
    return response['activities-heart-intraday']['dataset'][-1]

if __name__ == '__main__':
    start_time = time.time()
    while True:
        response = get_request('https://api.fitbit.com/1/user/-/activities/heart/date/today/1d/1sec.json',
                               'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyMzg2Q1EiLCJzdWIiOiI5WE5RUlkiLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJzY29wZXMiOiJyaHIiLCJleHAiOjE2NTA4NjYyNTEsImlhdCI6MTY1MDI2MTQ1MX0.MMfg_4I-nSMLnEHpvpgSgWaTXLTIWUVHG0NyEz40M84')
        current_hr = current_heart_rate(response)
        print(response)
        print(current_hr['value'])
        time.sleep(30 - ((time.time() - start_time) % 30))
