import time
import cv2
import fitbit
from multiprocessing import Process, Manager, Queue
from pythonosc.udp_client import SimpleUDPClient
from GazeTracking.gaze_tracking import GazeTracking
from GazeTracking.eye_tracking import gaze_track
from GazeTracking.load import load_model, predict 
from normalize import normalize, clip

def send_osc(ip, port, addr, msg):
    client = SimpleUDPClient(ip, port)
    client.send_message(addr, msg)


def update_hr(start_time, update_time, queue):
    while True:
        response = fitbit.get_request('https://api.fitbit.com/1/user/-/activities/heart/date/today/1d/1sec.json',
                        'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyMzg2Q1EiLCJzdWIiOiI5WE5RUlkiLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJzY29wZXMiOiJyaHIiLCJleHAiOjE2NTE4MzM5MjEsImlhdCI6MTY1MTgyMDc3M30.AAUtqsxESanPAhj8LhSQPUdgjcYCcpz_MRvETpVe-Qg')
        heart_rate = fitbit.current_heart_rate(response)
        print(f"Hear Rate is: {heart_rate['value']}")
        hr_normalized = clip(0, normalize(heart_rate['value'], 60, 80), 1)
        queue.put(hr_normalized)
        queue.put(heart_rate['value'])
        time.sleep(update_time - ((time.time() - start_time) % update_time))
        

def user_activity(queue):
    model = load_model('./GazeTracking/model.joblib')
    gaze = GazeTracking()
    webcam = cv2.VideoCapture(0)
    hr_normalized = 0
    heart_rate = 0
    while True:
        gaze_vel, right, left, top, bottom, center = gaze_track(gaze, webcam)
        if type(gaze_vel) is float:
            prediction = predict(model, gaze_vel)
            while not queue.empty():
                hr_normalized = queue.get()
                heart_rate = queue.get()

            user_activity = prediction.tolist()[0] * hr_normalized
            user_activity = normalize(user_activity, 0, 3)
            print(f"User activity: {user_activity}")
            send_osc('127.0.0.1', 5005, '/user_activity', user_activity)
            send_osc('127.0.0.1', 5005, '/heart_rate', heart_rate)
            send_osc('127.0.0.1', 5005, '/eye_categories',
                     prediction.tolist()[0])
            if right:
                send_osc('127.0.0.1', 5005, '/direction', "right")
            elif left:
                send_osc('127.0.0.1', 5005, '/direction', "left")
            elif top:
                send_osc('127.0.0.1', 5005, '/direction', "top")
            elif bottom:
                send_osc('127.0.0.1', 5005, '/direction', "bottom")
            elif center:
                send_osc('127.0.0.1', 5005, '/direction', "center")
        
        if cv2.waitKey(1) == 27:
            break
    webcam.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
        q = Queue()

        p1 = Process(target=update_hr, args=(time.time(), 20, q))
        p1.start()
        p2 = Process(target=user_activity, args=(q,))
        p2.start()
